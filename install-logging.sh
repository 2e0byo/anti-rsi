#!/bin/bash
ln -rs ./push_log.sh ~/bin/
mkdir -p ~/.config/systemd/user/
rm ./systemd/log-usage.service
cat<<EOF >./systemd/log-usage.service
[Unit]
Description=Push Usage Logs up to server

[Service]
ExecStart=$HOME/bin/push_log.sh


[Install]
WantedBy=default.target
EOF
ln -rs ./systemd/log-usage.service ~/.config/systemd/user/
ln -rs ./systemd/log-usage.timer ~/.config/systemd/user/
systemctl --user enable log-usage.service
systemctl --user enable log-usage.timer
systemctl --user start log-usage.timer
