#!/bin/bash

if [ ! -f "$HOME/.active_time" ]; then
    echo "No log file to push"
    exit 1
fi

server_backup_dir=volatile-tmp/ # set to some dir which won't fill up

scp $HOME/.active_time 2e0byo:logging/$HOSTNAME.active-time.log
# backup file
scp $HOME/.active_time 2e0byo:$server_backup_dir/$HOSTNAME.$(date '+%Y-%m-%d').active-time.log

