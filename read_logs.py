#!/usr/bin/python3
import csv
import glob
import os
from datetime import datetime, timedelta

import matplotlib.pyplot as plt
import mpld3
from matplotlib import rcParams

rcParams.update({"figure.autolayout": True})

logging = os.path.expanduser("~/logging")

hosts = {}
maxh = 12  # value to cap anomalies at (sometimes hardware is 'active' even when not in use)

for i in glob.glob(logging + "/*.active-time.log"):
    hostname = i.replace(".active-time.log", "").replace(logging, "").strip("/")
    hosts[hostname] = i  # why not?

for host in hosts:
    data = {}
    with open(hosts[host], "r") as f:
        logreader = csv.reader(f, delimiter=" ")
        for row in logreader:
            try:
                day = datetime.strptime(row[0], "%Y-%m-%d:")
                time = datetime.strptime(row[1], "%H:%M:%S")
                time = timedelta(
                    hours=time.hour, minutes=time.minute, seconds=time.second
                )
                if day not in data:
                    data[day] = timedelta(0)
                if time < timedelta(hours=maxh):
                    data[day] = max([data[day], time])
                else:
                    print("Anomaly:", day, time)
                    # data[day] = timedelta(hours=maxh)  # anomaly
                    data[day] = timedelta(0)
                    data[day] = timedelta(0)
            except (ValueError, IndexError):
                continue
            hosts[host] = data

# extend all to same range
min_date = min([min(hosts[host].keys()) for host in hosts])
max_date = max([max(hosts[host].keys()) for host in hosts])

for host in hosts:
    s = min_date
    while s <= max_date:
        if s not in hosts[host].keys():
            hosts[host][s] = timedelta(seconds=0)
        s += timedelta(days=1)
    sorted_keys = sorted(
        hosts[host].keys()
    )  # don't actually need to do this for each one
    hours = [hosts[host][x].seconds / 3600 for x in sorted_keys]
    plt.plot(list(sorted_keys), hours, label=host, linewidth=3, alpha=0.6)

aggregate = {}
s = min_date
while s <= max_date:
    aggregate[s] = sum([hosts[host][s] for host in hosts], timedelta())
    s += timedelta(days=1)

sorted_keys = sorted(aggregate.keys())
hours = [aggregate[x].seconds / 3600 for x in sorted_keys]

plt.plot(list(sorted_keys), hours, label="Total")

plt.title("Computer Usage")
plt.ylabel("Time/hours")
plt.xlabel("Date")
plt.legend()
plt.gcf().autofmt_xdate()
html_graph = mpld3.fig_to_html(plt.gcf())
# nasty hack to make it auto-scale; this will do for now
html_graph = """
<script>
var width = window.innerWidth
|| document.documentElement.clientWidth
|| document.body.clientWidth;

var height = window.innerHeight
|| document.documentElement.clientHeight
|| document.body.clientHeight;
</script>
""" + html_graph.replace(
    '"width": 640.0', '"width": width'
).replace(
    '"height": 480.0', '"height": height'
)
with open(os.path.expanduser("~/web/logs/usage.html"), "w") as f:
    f.write(html_graph)
# plt.show()
