#+Title: A basic python script to enforce ergonomic working habits

~anti-rsi.py~ is a basic script to force ‘micro’ and ‘macro’ breaks
ever so often.  It has the following features:

- Discounts system idle time (so you can walk away without stopping
  it)
- Permits postponing micro-breaks (with increasing penalties)
- Pausable, with warnings when paused
- Simple code, all contained in one file
- Minimal overhead

* Pausing/Postponing/Forcing
To pause send ~SIGUSR1~ to the script; to postpone send ~SIGUSR2~.  Or
call ~rsi-pause-unpause.sh~ or ~rsi-postpone.sh~ (e.g. from Rofi, or
bind to a key) which do exactly that.

To force a macro break, send ~SIGALRM~ (which hardly anyone ever uses,
anyhow), or use ~rsi-force.sh~.  This should be done sparingly: the
idea is to make you break /when you’re at the computer/, not when
you’re doing something else: force-breaking every time you go away is
cheating! (But is really useful sometimes, when it’s more convenient
to stop /now/.)  Forcing just triggers a break the usual way,
resetting the timers.

* Why another?

There are quite a few anti-rsi programs out there.  Why another?  None
of the available solutions I came across was simple and easily
modifiable, and all looked awful.  Plus this one /really/ takes
account of idle time: you won’t wake up to find an hour’s breaks
scheduled.

* Usage Logging

[[700xcomputer_usage.jpg]]

Since we’re watching idle time, it made sense to add a usage logger
which keep track of how long you’re interacting with the computer each
day, logging it to ~logf~ and writing it out every second to
~/dev/shm/usage~ (shm=shared memory=ramdisk), so you can pick it up
with i3bar or the like if you want to display it somewhere (personally
I use a wrapper around i3status and i3bar).  We trap ~SIGINT~ and
~SIGTERM~ and save before going.  But even then, I had it die a few
times without warning, probably because I killed something.  So for
now it backs up every 2 minutes to ~logf~ until I can track the
problem down.

As a consequence I have a lot of logs lying around, so I wrote some
code to push them up to a server daily, and some more code to parse
and plot.  Usage should be fairly straight-forward.

* Install

You need some kind of idle time printer.  See [[http://orgmode.org/manual/Resolving-idle-time.html#fnd-1][emacs org's idle time]] or
[[https://stackoverflow.com/questions/3208450/get-mac-idle-time-c-or-python][Stackoverflow]] for hints: on debian ~xprintidle~ can be installed.
~notify2~ is avaible from pip: for mac/windows there are other
libraries.

For the logging/pushing code, make sure you can ssh to the server,
edit the hostname in ~push_log.sh~ and run ~./install-logging.sh~ on
each host (requires systemd).
