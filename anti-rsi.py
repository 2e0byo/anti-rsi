#!/usr/bin/env python3
import os.path
import signal
from datetime import date, timedelta
from re import findall
from subprocess import call, check_output, run
from sys import exit
from threading import Thread
from time import monotonic, sleep

import notify2

# --------------------------------------------------------------------
# configuration variables

micro_break_frequency = 20  # minutes
micro_break_duration = 20  # seconds
micro_break_postpone = [60, 45, 20]  # seconds
max_micro_postpones = 3
macro_break_frequency = 60  # minutes
macro_break_duration = 5  # minutes
macro_break_postpone = [5, 3]  # minutes
max_macro_postpones = 2
paused_warning_frequency = 5  # minutes
significant_idle_time = 60  # seconds
logfile = os.path.expanduser("~/.active_time")
poll_file = "/dev/shm/usage"

draft = False
if draft is True:
    significant_idle_time = 2
# --------------------------------------------------------------------

notify2.init("anti-rsi")
break_in_progress = False
unfaded_brightness = 1
dim_brightness = 0.5


def backup_and_quit():
    print("exiting safely")
    try:
        save_daily_usage(today, active_seconds)
    except:
        pass
    exit()


def term(arg1, arg2):
    backup_and_quit()


def kill(arg1, arg2):
    backup_and_quit()


signal.signal(signal.SIGTERM, term)
signal.signal(signal.SIGINT, kill)


def draft_print(something):
    """Saves commenting out every 'print' statement which was only put in for
    testing."""
    global draft
    if draft is True:
        print(something)


def popup(title, message, timeout):
    """
    call notify-send with optional timeout (set to 0 to ignore).

    Recode on other platforms
    """
    pop = notify2.Notification(title, message)
    pop.set_urgency(notify2.URGENCY_CRITICAL)
    pop.set_timeout(timeout * 1000)
    pop.show()
    return pop


def get_idle_time():
    idle_time = check_output(["xprintidle"])
    return int(idle_time) / 1000


def lock_screen():
    """
    Lock Screen to indicate a macro_break has begun.  This is vastly preferable to
    actually preventing input (e.g. via xinput), as.

    1. if it goes wrong we aren't forced to ssh in or reboot unsafely

    2. it can be ignored when absolutely necessary, *but* a nasty red
       popup will scare you the whole time and

    3. it can be postponed if required: this is essential when e.g. giving
       a lecture, or looking something up for someone.  Although in these
       cases you could *pause* the script, you probably forgot.  Limiting
       mouse usage to a particular window (or keyboard) is far more
       complicated.

    4. it's sufficiently annoying to make taking the break likely.
    """
    call(
        ["fish", "-c", "lock"]
    )  # or actually put the lock commands here, but this is my universal
    # standard method


def fade_screen_brightness(start, end, dim_seconds=1):
    """Fade screen in one direction or the other, to indicate or end a micro break."""
    screens = [
        x.lstrip().split(" ")[0]
        for x in check_output("xrandr", encoding="utf8").split("\n")
        if "connected" in x
    ]
    n = 0
    brightness = start
    while brightness != end:
        if stop_fade is True:
            return
        n += 1
        dim_steps = 100
        sleep(dim_seconds / dim_steps)
        if brightness > end:
            brightness = start - (n / dim_steps)
            if brightness < end:
                brightness = end
        else:
            brightness = start + (n / dim_steps)
            if brightness > end:
                brightness = end
        for screen in screens:
            run(["xrandr", "--output", screen, "--brightness", str(brightness)])


def get_screen_brightness():
    """Get max screen brightness from xrandr."""
    brightness = 0
    for line in check_output(["xrandr", "--verbose"], encoding="utf8").split("\n"):
        try:
            found_brightness = float(
                findall(r".*Brightness\: ([0-9]+\.[0-9]*)", line)[0]
            )
            brightness = max(brightness, found_brightness)
        except IndexError:
            continue
    return brightness


def micro_break():
    global break_in_progress, current_popup, break_to_go, stop_fade
    global unfaded_brightness, dim_brightness
    break_in_progress = "micro"
    current_popup = popup("Micro Break", "rest eyes and hands!", micro_break_duration)
    stop_fade = False
    unfaded_brightness = get_screen_brightness()
    dim_brightness = unfaded_brightness - 0.5 if unfaded_brightness > 0.6 else 0.3
    Thread(
        target=fade_screen_brightness, args=(unfaded_brightness, dim_brightness)
    ).start()
    break_to_go = micro_break_duration


def macro_break(*args):
    global break_in_progress, current_popup, break_to_go
    break_in_progress = "macro"
    current_popup = popup(
        "Macro Break", "make a cup of tea!", (macro_break_duration * 60)
    )
    lock_screen()
    break_to_go = macro_break_duration * 60


def postpone(force=False, *args):
    global break_in_progress, micro_postpones, time_to_micro_break
    global macro_postpones, time_to_macro_break, stop_fade
    global dim_brightness, unfaded_brightness

    if break_in_progress == "micro":
        if (micro_postpones <= max_micro_postpones) or (force is True):
            try:  # increasing penalties
                time_to_micro_break = micro_break_postpone[micro_postpones]
            except IndexError:
                time_to_micro_break = micro_break_postpone[-1]
            current_popup.close()
            break_in_progress = False
            micro_postpones += 1
            draft_print("postponing micro")
        stop_fade = True
        sleep(1 / 100)  # make sure fade exits
        stop_fade = False
        Thread(
            target=fade_screen_brightness, args=(dim_brightness, unfaded_brightness)
        ).start()

    elif break_in_progress == "macro":
        if (macro_postpones <= max_macro_postpones) or (force is True):
            try:
                time_to_macro_break = macro_break_postpone[macro_postpones] * 60
            except IndexError:
                time_to_macro_break = macro_break_postpone[-1]
            current_popup.close()
            break_in_progress = False
            macro_postpones += 1
            draft_print("postponing macro")


def pause():
    global enable
    enable = False
    postpone(force=True)


def unpause():
    global enable
    enable = True


def toggle_pause(*args):
    global enable
    if enable is True:
        pause()
    else:
        unpause()


# --------------------------------------------------------------------
# usage logging functions


def get_usage_data():
    """
    Read usage data from file.

    Throws FileNotFoundError if no file.
    """
    with open(logfile, "r") as f:
        data = [x for x in f.readlines() if not x.strip() == ""]
    return data


def save_daily_usage(date, active_seconds, overwrite=False):
    """Append daily computer usage to logfile in human-readable format."""
    global logfile
    if overwrite is True:
        try:
            data = get_usage_data()
            if date.strftime("%Y-%m-%d: ") in data[-1]:
                data.pop()  # remove today
                with open(logfile, "w") as f:
                    f.writelines(data)
        except FileNotFoundError:
            pass
    with open(logfile, "a") as f:
        f.write(
            date.strftime("%Y-%m-%d: ") + str(timedelta(seconds=active_seconds)) + "\n"
        )


def polled_usage(arg1, arg2):
    out_usage(active_seconds)


def out_usage(active_seconds):
    """Write current usage to file, for i3bar or the like."""
    with open(poll_file, "w") as f:
        u = str(timedelta(seconds=active_seconds)).split(":")
        f.write("%sh%sm" % (u[0], u[1]))


# --------------------------------------------------------------------
# Main Loop

signal.signal(signal.SIGUSR1, toggle_pause)  # handle pauses and postpones
signal.signal(signal.SIGUSR2, postpone)
signal.signal(signal.SIGALRM, macro_break)  # force macro_break
signal.signal(signal.SIGPOLL, polled_usage)

time_to_micro_break = micro_break_frequency * 60
micro_postpones = 0
time_to_macro_break = macro_break_frequency * 60
macro_postpones = 0
enable = True
idle_time = 0
idle_watch = False
time_to_paused_reminder = paused_warning_frequency * 60
break_to_go = 0
active_seconds = 0
idle_seconds = 0
today = date.today()

time_to_micro_break = 1

# see if we crashed today and can accumulate
try:
    last = get_usage_data()[-1].strip("\n").split(": ")
    if last[0].strip() == str(today):
        t = last[1].split(":")
        s = int(t[0]) * 60 * 60 + int(t[1]) * 60 + int(t[2])
        active_seconds += s
except FileNotFoundError:
    pass
"""Main loop: decrement countdown timers to macro & micro break,
whilst incrementing system idle counter.  If idle counter goes above
significant level, add significant level to countdowns (i.e. don't
count idle time) and stop counting down until no longer idle).  If
macro elapses, pause countdowns and carry out macro break, then reset
countdowns and continue.  If micro elapses, ditto (mutatis mutandis).
If at macro elapse micro is less then 1 minute away, reset micro
countdown.

To get an idea how long we're using the computer for each day, we log
total elapsed time as well as

During breaks, calling postpone() will, in the case of micros,
postpone by breaktype_break_postpone if max_breaktype_postpones hasn't
been reached.  Calling pause() will postpone even if postpones
reached, and then pause execution of the main loop untill unpause() is
called.

"""
while True:
    start = monotonic()
    if date.today() - today > timedelta(days=0):  # new day
        save_daily_usage(today, active_seconds)
        today = date.today()
        active_seconds = 0
        idle_seconds = 0

    if (active_seconds % 120) == 0:  # save every 2 mins
        save_daily_usage(today, active_seconds, overwrite=True)
    active_seconds += 1
    out_usage(active_seconds)
    draft_print(active_seconds)
    if enable is False:
        draft_print("paused " + str(time_to_paused_reminder))
        time_to_paused_reminder -= 1
        if time_to_paused_reminder == 0:
            popup("Anti-RSI", "is paused", 1)
            time_to_paused_reminder = paused_warning_frequency * 60
        sleep(1)
        continue

    if break_in_progress is not False:
        if break_to_go == 0:
            if break_in_progress == "micro":
                micro_postpones = 0
                Thread(
                    target=fade_screen_brightness,
                    args=(dim_brightness, unfaded_brightness),
                ).start()
            else:
                macro_postpones = 0
            break_in_progress = False
            current_popup.close()
            continue
        break_to_go -= 1
        draft_print(break_to_go)
        sleep(1)
        continue

    idle_time = get_idle_time()
    if idle_time > significant_idle_time:
        if idle_watch is False:
            draft_print("went idle")
            time_to_macro_break += significant_idle_time
            time_to_micro_break += significant_idle_time
            idle_watch = True
        idle_seconds += 1
        try:
            sleep(1 + start - monotonic())
        except ValueError:
            print("Skipped! What's the load?")
        continue
    elif idle_watch is True:
        idle_watch = False  # start counting again
        active_seconds -= idle_seconds + 1  # subtract idle time from count
        draft_print(active_seconds)
        if draft is True:
            save_daily_usage(today, active_seconds)
        idle_seconds = 0
        draft_print("no longer idle")
    time_to_macro_break -= 1
    draft_print("macro: " + str(time_to_macro_break))
    time_to_micro_break -= 1
    draft_print("micro: " + str(time_to_micro_break))
    # macro elapses
    if time_to_macro_break == 0:
        draft_print("macro break")
        macro_break()
        time_to_macro_break = macro_break_frequency * 60
        if time_to_micro_break < 60:
            time_to_micro_break = micro_break_frequency * 60
            draft_print("rescheduling micro break")
    # micro elapses
    if time_to_micro_break == 0:
        draft_print("micro break")
        micro_break()
        time_to_micro_break = micro_break_frequency * 60
    try:
        sleep(1 + start - monotonic())
    except ValueError:
        print("Skipped! What's the load?")
